#include "myshell.h"

std::string path =""; /* global var for current path */

/* returns linux if user is using linux
   returns unidentified otherwise */
std::string getOsName(){ 
#ifdef __linux__
  return "Linux";
#else
  return "unidentified";
#endif
}

/* enum for commands to be used in a switch */
enum Commands{
	      cd,
	      cls,
	      dir,
	      cpy, /* cpy because "copy" is a reserved name */
	      print,
	      md,
	      rd,
	      quit,
	      invalid,
};
  
/* returns int val for switch based on user input */
Commands hashCommand(std::string input){
  if (input == "cd") return cd;
  if (input == "cls") return cls;
  if (input == "dir") return dir;
  if (input == "copy") return cpy;
  if (input == "print") return print;
  if (input == "md") return md;
  if (input == "rd") return rd;
  if (input == "quit") return quit;
  return invalid; 
}

/* function that removes the first 5 chars from a string.
   used to remove /home from a path */
string removeHome(string path){
  return path.substr(5, path.back());
}

/* function that returns true or false depending on whether
   a file path exists */
bool dirExists(string dir){
  DIR* directory;
  directory = opendir(dir.c_str());

  if (directory){
    closedir(directory);
    return true;
  } else {
    return false;
  }
}

void clear(){ /* basic clear function */
  system("clear");
}

/* main command for checking user input or file input */
bool runCommands(std::string userInput) {

  std::string command, argSubStr, argument, argument2;
  
  bool quitCheck = false;
  
  std::string file;
  
  command = userInput.substr(0, userInput.find(' ')); /* command is the entered string up until the first space */

  if (userInput.find(" ") != string::npos){ /* if there is at least one space in the user input */

    argSubStr = userInput.substr(userInput.find(' ') + 1, userInput.back()); /* substr from first space to end of input line */
      
    argument = argSubStr.substr(0, argSubStr.find(' ')); /* subtr from previous substr to first space */

    argument2 = argSubStr.substr(argSubStr.find(' ') + 1, argSubStr.back()); /* for functions that require two inputs after the command (e.g. copy) */
  } 
  
  switch(hashCommand(command)){
  
  case cd:
    if (argument == "" || argument == " "){ /* if the user put no argument after cd */
      /* does not change current working directory */
      cout << "cwd : " << path << "\n";
      break;
    }
    if (argument == ".." && path != "/home"){ /* if user inputs ".." after cd and current path is not home */
      path = path.substr(0, path.find_last_of('/')); /* take the last directory off of the current path */
      cout << "cwd : " << path << "\n";
      break;
    } else if (argument == ".." && path == "/home"){ /* do nothing if user is currently on /home */
      break;
    }

    if (dirExists(argument)){ 
      path = argument; /* if the user enters a directory, and it exists, change path to equal that directory */
      cout << "cwd : " << path << "\n";
    } else {
      cout << "Cannot find directory " << argument << ".\n";
    }

    break;
      
  case cls: /* calls the clear function */
    clear();
      
    break;
    
  case dir:
  /* check that argument directory exists, if it does, it loops through all folders
	 and files, and prints them */
      if(dirExists(argument)){
	for (const auto & entry : experimental::filesystem::directory_iterator(argument)){
    
	  file = (string) entry.path(); /* casts each entry to a string for printing */
      
	  /* prints all files+folders */
	  cout <<  file.substr(file.find_last_of('/') + 1, file.back()) << "  ";
	}
	cout << "\n";
      } else { /* otherwise reports that argument was not found */
	cout << "Cannot find directory " << argument << ".\n";
      }
      
    break;
    
  case cpy:
  
      /* copies inputted directory to destination */
      experimental::filesystem::copy(path + "/" + argument, path + "/" + argument2, std::experimental::filesystem::copy_options::recursive);
    break;
    
  case print:
      /* prints the argument after print */
      std::cout << argSubStr << "\n";
    break;
    
  case md:
  
    /* creates a directory*/
    experimental::filesystem::create_directories(path +"/" + argument);
    break;
    
  case rd:
   try {
    /* removes folder in given path */
    experimental::filesystem::remove(path + "/"  +argument);
    
    } catch (const experimental::filesystem::filesystem_error& e){ /*catch filesystem error if folder is not empty */
      cout << argument << " is not empty. It cannot be removed." << "\n";
      break;
	}
    break;
    
 
  case quit:
    /* sets quit flag to true and breaks the switch */
    quitCheck = true;
    break;
    
  default:
    /* if command is not recognised */
    std::cout << command << " does not exist as a function in this shell.\n";
    break;
  }

  return quitCheck; /* if quitCheck is true, the main loop breaks and the program ends */
}

/* main loop function */
void loop(std::string fileToRead){
  std::string userInput;
  std::string command;
  std::string argument;
  std::string readFile;
  bool quitTime = false;

  /* puts current working directory into path string */
  path+= experimental::filesystem::current_path();
  
  while(!quitTime){

    if (fileToRead != "") { /* checks if user has run the program with a file as an argument */
      ifstream readingTheFile(fileToRead); /* opens file stream */

      while (getline (readingTheFile, readFile)) { /* while there are still lines in the file */ 

	    quitTime = runCommands(readFile); /* passes the lines in the file to the runCommands function */
	
      }
      break;
    }
      
    /* prints current user and current file path (excluding /home)
       this is to mimic a shell prompt */
    cout << getenv("USER") << "@" << getOsName() << ":~"<< removeHome(path) << "$ ";
   
    std::getline(std::cin, userInput); /* if there is no file to read, it instead takes user input */
    
    quitTime = runCommands(userInput); /* pass userInput to runCommands */
  }
}


int main(int argc, char** argv) { 

  /* Collects the arguments given by the user from command line */
  std::string fileSource = "";

  if (argc > 1){ /* Check to see if any other words have been typed in at command line */
    fileSource = argv[1]; /* gets the name of the first item after the program name (from command line) */
  }

  loop(fileSource);

}
