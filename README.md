**CST 2555 - Operating System and Computer Networks**

Coursework 1 – Operating System – 20% of final mark

*A project adopted from 'William Stalling, Simple Projects' designed by Dr Ian Graham.*

---

## Introduction

A program that simulates the Ubuntu terminal and a basic set of linux commands. Allows the user
to perform the following functionalities:

* cd <directory> = change directory
* cls = clear screen
* dir <directory> = list directory content
* copy <directory> <directory2> = copy directory
* print <comment> = displays comment
* md <directory> = make directory
* rd <directory> = remove directory
* quit = exit program/shell


---

## Installation

Requirements:

	* Linux
	* Emacs (version 11 or higher)

Install using: 
	
	$ sudo apt-get install emacs

The libraries & modules required for the program include:

	* <windows.h>
	* <limits.h>
	* <sys/stat.h>
	* <stdlib.h>
	* <dirent.h>
	* <typeinfo>
	* <fstream>
	* <filesystem>
	* <experimental/filesystem>

---

## How to use?

**cd** - To change the directory, input cd and the specific directory (full file path) you would like to enter. E.g *cd /home/folder1*

**cls** - To clear the terminal, enter cls alone. E.g *cls*

**dir** - To list directory content, input dir and the directory (full file path) you would like to view (both files + folders). E.g *dir /home/folder1*

**copy** - To copy the contents of one directory to another, input cpy, the directory you would like to copy and the destination directory following this. E.g *copy folder1 folder2*

**print** - To print a comment, input print and the comment you would like to appear in the terminal. E.g *print Hello World*

**md** - To make a directory, input md followed by the name you would like to give the directory. E.g *md newFolder*

**rd** - To remove a directory, input rd and the directory name. E.g *rd newFolder*

**quit** - To quit the program/shell, simply enter quit and nothing else. E.g *quit*

Before you run the program, you must first compile it using the make command.

Additionally, if reading from an external file, compile with: './myshell <filename>' (else follow the normal procedure).

---

## Troubleshooting

If a number of the system commands do not work:
	
	- Ensure your account has been given admin privileges.

	- Check that you have the latest version of Emacs.
	
If the file reading does not work:
	
	- Make sure the file exists in the current working directory.
	
	- Ensure all spellings are correct for the program to read it.

---

## FAQ

Q: How do I use the program?

	A: Choose one of the commands you would like to perform and enter the shortcut. For instance,
   	  in order to make a directory, enter 'md' followed by the name you'd like to give it. Another
   	  example is clearing the terminal. Simply enter 'cls' without anything preceding/following it
   	  and hit enter. (Refer to 'How to use' for more information).
    

Q: How to compile without the makefile?

	A: g++ -o <filename> main.cpp myshell.cpp -lstdc++fs
	

Q: What if I input it incorrectly?

	A: The program takes the input and retrieves the specific command using a hashmap. If the input
    is invalid for whatever reason, it will return nothing; give a warning message and prompt for
    another input from the user.
	  
---

## Contribution

* Samuel Donovan (M00521789)
* Mahdi Ahbab (M00694421)
* Jason Adoteye (M00659794)
* Gabriel Garcia (M00674790)
* Lukas Urba (M00674654)
